/*
 * Peter Haschke
 * 12/8/16
 * Project 5`: Image viewer
 * !!COMPILED WITH GCC ON UBUNTU 16.04!!
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <ctype.h>
#include </usr/include/GLFW/glfw3.h>
#include </usr/include/GL/gl.h>
#include "linmath.h"

#define DELTA_PAN_X 0.1
#define MAX_ABS_PAN_X 1.9
#define DELTA_PAN_Y 0.1
#define MAX_ABS_PAN_Y 1.9

#define DELTA_SHEAR_X 0.1
#define MAX_ABS_SHEAR_X 4.0
#define DELTA_SHEAR_Y 0.1
#define MAX_ABS_SHEAR_Y 4.0

#define DELTA_ROTATE_DEGS 15

#define DELTA_ZOOM_OUT 0.9
#define DELTA_ZOOM_IN 1.1
#define MAX_ZOOM 10

typedef struct pixelStruct {
        unsigned char r, g, b;
} Pixel;

typedef struct VertexStruct {
        float Position[2];
        float TexCoord[2];
} Vertex;

// (-1, 1) (1, 1)
// (-1, -1) (1, -1)

//Default vertices.
Vertex vertices[] = {
        {{1, 1}, {0.999, 0}},
        {{-1, 1}, {0, 0}},
        {{-1, -1}, {0, 0.999}},

        {{-1, -1}, {0, 0.999}},
        {{1, 1}, {0.999, 0}},
        {{1, -1}, {0.999, 0.999}}
};

//Variables for affine transformations.
float kScale=1.0, horPos=0.0, vertPos=0.0, rotAngleDegs=0.0, shearX=0.0, shearY=0.0;

static const char* vertexShaderText =
"uniform mat4 MVP;\n"
"attribute vec2 TexCoordIn;\n"
"attribute vec2 vPos;\n"
"varying vec2 TexCoordOut;\n"
"void main()\n"
"{\n"
"    gl_Position = MVP * vec4(vPos, 0.0, 1.0);\n"
"    TexCoordOut = TexCoordIn;\n"
"}\n";

static const char* fragmentShaderText = 
"varying mediump vec2 TexCoordOut;\n"
"uniform sampler2D Texture;\n"
"void main()\n"
"{\n"
"    gl_FragColor = texture2D(Texture, TexCoordOut);\n"
"}\n";

//Function Prototypes
void convertPixels(Pixel *pixelBuffer, int numPixels, unsigned char* image);
void skipComments(FILE* fdIn);
void readAscii(FILE* fdIn, int numPixels, Pixel *pixelBuffer);
void readBinary(FILE* fdIn, int numPixels, Pixel *pixelBuffer);
void getBuffer(FILE* fdIn, Pixel** pixelBuffer, int* width, int* height);
void skipWS(FILE* fdIn);
int nextChar(FILE* fdIn);
int getNumber(FILE* fdIn);
static void errorCallback(int error, const char* description);
static void keyCallback(GLFWwindow*, int key, int scancode, int action, int mods);
void glCompileShaderOrDie(GLuint shader);
void glLinkProgramOrDie(GLuint program);

int main(int c, char** argv) {
       
        GLuint vertexBuffer, vertexShader, fragmentShader, program;
        GLint mvpLocation, vposLocation, vcolLocation;
        float imageRatio;

        printf("--KEYBOARD CONTROLS--\n");
        printf("QUIT - ESC\n");
        printf("PAN - W/A/S/D or UP/DOWN/LEFT/RIGHT\n");
        printf("SHEAR - HORIZONTAL - F/H\n");
        printf("SHEAR - VERTICAL - T/G\n");
        printf("ROTATE - CLOCKWISE - R\n");
        printf("ROTATE - COUNTERCLOCKWISE - E\n");
        printf("ZOOM IN - X\n");
        printf("ZOOM OUT - C\n");
        printf("RESET IMAGE - Q\n");
        printf("----------------------------------------\n\n");

        if(c != 2){
                fprintf(stderr, "Error: not enough arguments, expected ./ezview input.ppm\n");
                exit(1);
        }

        FILE* fdIn;
        fdIn = fopen(argv[1], "r");
        if(fdIn == NULL){
                fprintf(stderr, "Error: Cannot open file for reading.\n");
                exit(1);
        }

        Pixel* pixelBuffer;
        int width, height;

        getBuffer(fdIn, &pixelBuffer, &width, &height);

        int numPixels = width*height;
        unsigned char* image = (unsigned char*)malloc(sizeof(unsigned char)*numPixels*4);  
        convertPixels(pixelBuffer, numPixels, image);

        //Handle non-square images.
        if(height > width){
                imageRatio = (float)width/(float)height;
                vertices[0].Position[0] = imageRatio;
                vertices[1].Position[0] = -imageRatio;
                vertices[2].Position[0] = -imageRatio;
                vertices[3].Position[0] = -imageRatio;
                vertices[4].Position[0] = imageRatio;
                vertices[5].Position[0] = imageRatio;
        }
        if(width > height){
                imageRatio = (float)height/(float)width;
                vertices[0].Position[1] = imageRatio;
                vertices[1].Position[1] = imageRatio;
                vertices[2].Position[1] = -imageRatio;
                vertices[3].Position[1] = -imageRatio;
                vertices[4].Position[1] = imageRatio;
                vertices[5].Position[1] = -imageRatio;
        }

        glfwSetErrorCallback(errorCallback);
        if(!glfwInit()) return -1;

        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

        GLFWwindow* window = glfwCreateWindow(640, 480, "EZview", NULL, NULL);

        if(!window){
                glfwTerminate();
                fprintf(stderr, "Error: Unable to create window.\n");
                exit(1);
        }

        glfwSetKeyCallback(window, keyCallback);

        glfwMakeContextCurrent(window);

        glfwSwapInterval(1);

        glGenBuffers(1, &vertexBuffer);
        glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

        vertexShader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertexShader, 1, &vertexShaderText, NULL);
        glCompileShaderOrDie(vertexShader);

        fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragmentShader, 1, &fragmentShaderText, NULL);
        glCompileShaderOrDie(fragmentShader);


        program = glCreateProgram();
        glAttachShader(program, vertexShader);
        glAttachShader(program, fragmentShader);
        glLinkProgramOrDie(program);

        mvpLocation = glGetUniformLocation(program, "MVP");
        assert(mvpLocation != -1);

        vposLocation = glGetAttribLocation(program, "vPos");
        assert(vposLocation != -1);

        GLint texcoordLocation = glGetAttribLocation(program, "TexCoordIn");
        assert(texcoordLocation != -1);

        GLint texLocation = glGetUniformLocation(program, "Texture");
        assert(texLocation != -1);

        glEnableVertexAttribArray(vposLocation);
        glVertexAttribPointer(vposLocation,
                2,
                GL_FLOAT,
                GL_FALSE,
                sizeof(Vertex),
                (void*) 0);
        
        glEnableVertexAttribArray(texcoordLocation);
        glVertexAttribPointer(texcoordLocation,
                2,
                GL_FLOAT,
                GL_FALSE,
                sizeof(Vertex),
                (void*) (sizeof(float) * 2));

        GLuint texID;
        glGenTextures(1, &texID);
        glBindTexture(GL_TEXTURE_2D, texID);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texID);
        glUniform1i(texLocation, 0);

        while (!glfwWindowShouldClose(window)){
                float ratio;
                int winWidth, winHeight;
                mat4x4 m, p, mvp, m1, shearM;

                glfwGetFramebufferSize(window, &winWidth, &winHeight);
                ratio = winWidth / (float) winHeight;

                glViewport(0, 0, winWidth, winHeight);
                glClear(GL_COLOR_BUFFER_BIT);

                mat4x4_identity(m1);

                mat4x4_rotate_Z(m, m1, rotAngleDegs*(M_PI/180));

                mat4x4_scale_aniso(m1, m, kScale, kScale, 1);

                mat4x4_identity(shearM);
                
                shearM[1][0] = shearX;
                
                shearM[0][1] = shearY;

                mat4x4_mul(m, m1, shearM);

                m[3][0] = horPos;
                m[3][1] = vertPos;

                mat4x4_ortho(p, -ratio, ratio, -1.f, 1.f, 1.f, -1.f);
                mat4x4_mul(mvp, p, m);

                glUseProgram(program);
                glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*) mvp);
                glDrawArrays(GL_TRIANGLES, 0, 6);

                glfwSwapBuffers(window);
                glfwPollEvents();
        }
        glfwDestroyWindow(window);
        glfwTerminate();

        free(image);
        free(pixelBuffer);
        return 0;
}

void glLinkProgramOrDie(GLuint program) {
        GLint linked;
        
        glLinkProgram(program);
                glGetProgramiv(program,
                GL_LINK_STATUS,
                &linked);
        if (!linked) {
                GLint infoLen = 0;
                glGetProgramiv(program,
                        GL_INFO_LOG_LENGTH,
                        &infoLen);
        char* info = malloc(infoLen+1);
                GLint done;
                glGetProgramInfoLog(program, infoLen, &done, info);
                printf("Unable to link program: %s\n", info);
                exit(1);
        }
}

void glCompileShaderOrDie(GLuint shader) {
        GLint compiled;
        glCompileShader(shader);
                glGetShaderiv(shader,
                GL_COMPILE_STATUS,
                &compiled);
        if (!compiled) {
                GLint infoLen = 0;
                glGetShaderiv(shader,
                        GL_INFO_LOG_LENGTH,
                        &infoLen);
        char* info = malloc(infoLen+1);
                GLint done;
                glGetShaderInfoLog(shader, infoLen, &done, info);
                printf("Unable to compile shader: %s\n", info);
                exit(1);
        }
}

void convertPixels(Pixel *pixelBuffer, int numPixels, unsigned char* image){
        int i;
        Pixel* pixel;
        unsigned char* rawPt;
        for (i=0, pixel=pixelBuffer, rawPt=image; i<numPixels; i++, pixel++){
                        *rawPt++ = pixel->r;
                        *rawPt++ = pixel->g;
                        *rawPt++ = pixel->b;
                        *rawPt++ = 255;
        }
}


static void errorCallback(int error, const char* description){
        fprintf(stderr, "GLFW Error: %d %s\n", error, description);
}

//Keyboard controls.
static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
            if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
                            glfwSetWindowShouldClose(window, GL_TRUE);

            if((key == GLFW_KEY_A || key == GLFW_KEY_LEFT) && action == GLFW_PRESS){
                    horPos -= DELTA_PAN_X;
                    if(horPos < -MAX_ABS_PAN_X){
                        horPos = -MAX_ABS_PAN_X;
                    }
            }

            if((key == GLFW_KEY_D || key == GLFW_KEY_RIGHT) && action == GLFW_PRESS){
                    horPos += DELTA_PAN_X;
                    if(horPos > MAX_ABS_PAN_X){
                        horPos = MAX_ABS_PAN_X;
                    }
            }

            if((key == GLFW_KEY_S || key == GLFW_KEY_DOWN) && action == GLFW_PRESS){
                    vertPos -= DELTA_PAN_Y;
                    if(vertPos < -MAX_ABS_PAN_Y){
                        vertPos = -MAX_ABS_PAN_Y;
                    }
            }

            if((key == GLFW_KEY_W && action || key == GLFW_KEY_UP) == GLFW_PRESS){
                    vertPos += DELTA_PAN_Y;
                    if(vertPos > MAX_ABS_PAN_Y){
                        vertPos = MAX_ABS_PAN_Y;
                    }
            }

            if(key == GLFW_KEY_F && action == GLFW_PRESS){
                    shearX -= DELTA_SHEAR_X;
                    if(shearX < -MAX_ABS_SHEAR_X){
                        shearX = -MAX_ABS_SHEAR_X;
                    }
            }

            if(key == GLFW_KEY_H && action == GLFW_PRESS){
                    shearX += DELTA_SHEAR_X;
                    if(shearX > MAX_ABS_SHEAR_X){
                        shearX = MAX_ABS_SHEAR_X;
                    }
            }

            if(key == GLFW_KEY_G && action == GLFW_PRESS){
                    shearY -= DELTA_SHEAR_Y;
                    if(shearY < -MAX_ABS_SHEAR_Y){
                        shearY = -MAX_ABS_SHEAR_Y;
                    }
            }

            if(key == GLFW_KEY_T && action == GLFW_PRESS){
                    shearY += DELTA_SHEAR_Y;
                    if(shearY > MAX_ABS_SHEAR_Y){
                        shearY = MAX_ABS_SHEAR_Y;
                    }
            }

            if(key == GLFW_KEY_R && action == GLFW_PRESS){
                    rotAngleDegs -= DELTA_ROTATE_DEGS;
            }

            if(key == GLFW_KEY_E && action == GLFW_PRESS){
                    rotAngleDegs += DELTA_ROTATE_DEGS;
            }

            if(key == GLFW_KEY_C && action == GLFW_PRESS){
                    kScale *= DELTA_ZOOM_OUT;
            }

            if(key == GLFW_KEY_X && action == GLFW_PRESS){
                    kScale *= DELTA_ZOOM_IN;
                    if(kScale > MAX_ZOOM){
                        kScale = MAX_ZOOM;
                    }
            }

            if(key == GLFW_KEY_Q && action == GLFW_PRESS){
                kScale=1.0;
                horPos=0.0;
                vertPos=0.0;
                rotAngleDegs=0.0;
                shearX=0.0;
                shearY=0.0;
            }
            
}

void getBuffer(FILE* fdIn, Pixel** pixelBuffer, int* width, int* height){

        if(fgetc(fdIn) != 'P'){
                fprintf(stderr, "Error: Not a Supported File Type\n");
                exit(1);
        }
        int inputType;
 
        //Get the second part fo the magic number from the input file and verify it is a 3 or a 6.
        //Save value as a int in a variable. Error out if it's not of either type.
        char c = fgetc(fdIn);
        if(c == '3'){
                inputType = 3;
        }else if(c == '6'){
                inputType = 6;
        }else {
                fprintf(stderr, "Error: Not a Supported File Type\n");
                exit(1);
        }

        //printf("Input type: %d\n", inputType);

        skipWS(fdIn);
        skipComments(fdIn);
        skipWS(fdIn);
        
        //Get first returned value which is the width and conver it to an int to store.
        *width = getNumber(fdIn);
        skipWS(fdIn);

        //Get next non comment value which is the height.
        *height = getNumber(fdIn);
        skipWS(fdIn);
 
        //Get next non comment value which is the magic number.
        int maxNum = getNumber(fdIn);
        skipWS(fdIn);
 
        //Throw and error if the file uses 2 byte numbers instead of just 1.
        if(maxNum > 255){
                fprintf(stderr, "Error: Program only support 1 byte images");
                exit(1);
        }
 
        //Calculate the total number of pixels in the image.
        int numPixels = *width * *height;


        //Allocate pixel stuct buffer.
        *pixelBuffer = (Pixel *)malloc(sizeof(Pixel)*numPixels);

        //printf("height: %d, width: %d, maxNum: %d\n", *width, *height, maxNum);

        //Call proper functions to read in data into Pixel buffer and write it out.
        if(inputType == 3) {
                readAscii(fdIn, numPixels, *pixelBuffer);
        }else{
                readBinary(fdIn, numPixels, *pixelBuffer);
        }
        
        //printf("Read pixels into buffer.\n");
        fclose(fdIn);
        return;

}

//Function to read ascii characters in and store in the pixelBuffer as unsigned chars.
void readAscii(FILE* fdIn, int numPixels, Pixel *pixelBuffer){
	int i, pixelCount;
	for(i = 0; i < numPixels; i++){
                skipWS(fdIn);
		pixelBuffer->r = (unsigned char)getNumber(fdIn);
                skipWS(fdIn);
		pixelBuffer->g = (unsigned char)getNumber(fdIn);
                skipWS(fdIn);
		pixelBuffer->b = (unsigned char)getNumber(fdIn);

		pixelCount++;
		*pixelBuffer++;

                if(pixelCount != numPixels){
                        skipWS(fdIn);
                }

	}
        return;
}

//Function to read binary from the input file and store it in the pixelBuffer.
void readBinary(FILE* fdIn, int numPixels, Pixel *pixelBuffer){
        int i, pixelCount;
        size_t bytesRead;
        for(i = 0; i < numPixels; i++){
                fread(&pixelBuffer->r, 1, 1, fdIn);
                fread(&pixelBuffer->g, 1, 1, fdIn);
                fread(&pixelBuffer->b, 1, 1, fdIn);
                
                *pixelBuffer++;
                pixelCount++;
        }
        return;
}

int getNumber(FILE* fdIn) { 
        int value; 
        int read = fscanf(fdIn, "%d", &value); 
        if(read == 0){ 
                fprintf(stderr, "Failed to read number.\n"); 
        } 
        return value; 
} 

//Function to take in a fdIn thats at the start of a comment and read it into a file until it hits a newline character.
void skipComments(FILE* fdIn) {
        int c = nextChar(fdIn);
        while (c != EOF){
                if(c == '#'){
                        c = nextChar(fdIn);
                        while (c != EOF){
                                if(c == '\n' || c == '\r'){
                                        //printf("Skipped Comment.\n");
                                        break;
                                }
                                c = nextChar(fdIn); 
                        }
                        //fprintf(stderr, "Error: Prematurely reached EOF.\n");
                } else {
                        ungetc(c, fdIn);
                        return;
                }
                skipWS(fdIn);
                c = nextChar(fdIn);
                //printf("nextCharComment: '%c'\n", c);

        }
        fprintf(stderr, "Error: Reached end of file prematurely in skipComments.\n");
}

//skipWS() skips white space in the file.
void skipWS(FILE* fdIn) {
        int c = nextChar(fdIn);
        while (isspace(c)) {
                c = nextChar(fdIn);
        }
        //printf("Skipped whitespace.\n");
        ungetc(c, fdIn);
}

// nextChar() wraps the getc() function and provides error checking and line
// // number maintenance
int nextChar(FILE* fdIn) {
        int c = fgetc(fdIn);
        #ifdef DEBUG
                printf("nextChar: '%c'\n", c);
        #endif
        if (c == EOF) {
                fprintf(stderr, "Error: Unexpected end of file in nextChar.\n");
                exit(1);
        }
        return c;
}


