all:
	gcc ezview.c -o ezview -lGL -lGLU -lm -lX11 -lXxf86vm -lXrandr -lpthread -lXi -lglfw

clean: 
	-rm -f ezview
